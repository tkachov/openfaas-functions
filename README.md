# My OpenFaaS Functions

## Function hello-user

Parameters are passed as a JSON object in the request body.

### Example Request

```
{
    "name": "Kevin",
    "birthdate": "1990-12-24",
    "short_answer": true
}
```

`name` and `birthdate` are required. `birthdate` must be of the format "yyyy-MM-dd".
`short_answer` is optional and defaults to true.

### Invokations

Here are different way of invoking the function assuming that the function is called `hello-user` and runs on the local machine.

#### curl
`curl --data '{"name": "Kevin", "birthdate": "1990-12-24"}' http://127.0.0.1:8080/function/hello-user`

#### faas-cli
`echo '{"name": "Kevin", "birthdate": "1990-12-24"}' | faas-cli invoke hello-user`

### Example Response
`Your name is Kevin and your age is probably 28`

## Function mandelbrot

Generates a mandelbrot fractal.
Parameters are passed as query parameters of the http request.
The result if returned as a png picture either as a base64 string or html.

### Query Parameters

|**Parameter**|**Description**|
|-------------|---------------|
|`w`|Width of the image|
|`h`|Height of the image|
|`iter`|Maximum number of iterations for the algorithm|
|`html`|*Optional.* Return html instead of a base64 string. Defaults to false|

### Invokations

The following describes how to invoke the method and display the result

#### curl

`curl "http://127.0.0.1:8080/function/mandelbrot?w=1600&h=1200&iter=40" | base64 --decode | display`

`display` is part of the `imagemagick` image manipulation suite and `base64` is included in the GNU coreutils.

#### faas-cli

`echo "" | faas-cli invoke mandelbrot --query w=1600 --query h=1200 --query iter=40 | base64 --decode | display`

### Web Browser

Visit `http://127.0.0.1:8080/function/mandelbrot?w=1600&h=1200&iter=40&html=true`
