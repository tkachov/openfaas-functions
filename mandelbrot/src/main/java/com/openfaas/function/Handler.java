package com.openfaas.function;

import com.openfaas.model.IHandler;
import com.openfaas.model.IResponse;
import com.openfaas.model.IRequest;
import com.openfaas.model.Response;

import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.util.Base64;
import java.util.Map;
import java.io.ByteArrayOutputStream;
import java.io.IOException;


public class Handler implements com.openfaas.model.IHandler {

    public IResponse Handle(IRequest req) {
        Response res = new Response();
        Map<String, String> queryParams = req.getQuery();

        boolean returnHtml = Boolean.parseBoolean(queryParams.get("html"));

        int width, height, maxIterations;
        try {
            width = Integer.parseInt(queryParams.get("w"));
            height = Integer.parseInt(queryParams.get("h"));
            maxIterations = Integer.parseInt(queryParams.get("iter"));
        } catch (NumberFormatException e) {
            res.setStatusCode(400);
            res.setBody("Could not parse query params");
            return res;
        }

        BufferedImage image = new MandelbrotRenderer(width, height, maxIterations).render();

        byte[] bytes = new byte[0];
        try (ByteArrayOutputStream os = new ByteArrayOutputStream()) {
            ImageIO.write(image, "png", os);
            bytes = os.toByteArray();
        } catch (IOException e) {
            res.setStatusCode(500);
            res.setBody("Internal Error occured");
            return res;
        }

        String result = Base64.getEncoder().encodeToString(bytes);

        if (returnHtml) {
            result = convertToHtml(result);
        }

	    res.setBody(result);

	    return res;
    }

    private String convertToHtml(String base64) {
        return String.format("<html><body><img src=\"data:image/png;base64, %s\" /></body></html>", base64);
    }
}
