package com.openfaas.function;

import java.awt.*;
import java.awt.image.BufferedImage;

public class MandelbrotRenderer {

    private int width;
    private int height;
    private int maxIterations;

    public MandelbrotRenderer(int width, int height, int maxIterations) {
        this.width = width;
        this.height = height;
        this.maxIterations = maxIterations;
    }

    public BufferedImage render() {
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);


        for (int col = 0; col < width; col++) {
            for (int row = 0; row < height; row++) {
                double x0 = (double) col / width * 3.5 - 2.5;
                double y0 = (double) row / height * 3.0 - 1.5;

                double x = 0;
                double y = 0;
                double iteration = 0;

                while (x * x + y * y < (1 << 16) && iteration < maxIterations) {
                    double tmp = x * x - y * y + x0;
                    y = 2 * x * y + y0;
                    x = tmp;
                    iteration++;
                }

                int color;

                if (iteration < maxIterations) {
                    double log_zn = Math.log(x * x + y * y) / 2;
                    double nu = Math.log(log_zn / Math.log(2)) / Math.log(2);
                    iteration = iteration + 1 - nu;
                    color = Color.HSBtoRGB((float) iteration / maxIterations, 1f, 1f);
                } else {
                    color = Color.black.getRGB();
                }

                image.setRGB(col, row, color);
            }
        }

        return image;
    }
}
