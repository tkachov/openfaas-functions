import json
from datetime import date
from math import floor

def handle(req):
    # Example request: {"name": "Max", "birthdate": "2000-01-01", "short_answer": false}
    data = json.loads(req)

    name = data["name"]
    short_answer = data["short_answer"] if "short_answer" in data else True

    birthdate = date.fromisoformat(data["birthdate"])
    delta = date.today() - birthdate
    age = floor(delta.days / 365)
    
    result = "Your name is " + name + " and your age "
    
    if not short_answer:
        result += "(which was calculated with highly sophisticated, blockchain-based, cloud-native algorithms of the Zettabyte era powered by IoT and AI technologies and developed with agile processes) "

    result += "is probably " + str(age)
    return result
